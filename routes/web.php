<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return view('auth.login');
});
Route::post('post-login', [App\Http\Controllers\UsersController::class, 'postLogin'])->name('login.post');
Route::post('register', [App\Http\Controllers\UsersController::class, 'register'])->name('employees_register');

Route::group(['middleware'=>['auth']],function(){
    Route::get('/home', [App\Http\Controllers\UsersController::class, 'index'])->name('home');
    Route::get('/profile', [App\Http\Controllers\UsersController::class, 'profile'])->name('profile');
    Route::post('/profile/{id}', [App\Http\Controllers\UsersController::class, 'profileUpdate'])->name('profile-update');
    Route::resource('leaves', App\Http\Controllers\LeaveController::class);
    Route::get('/notificationList', [App\Http\Controllers\UsersController::class, 'notificationList'])->name('notificationList');
});

Route::group(['middleware'=>['auth','admin']],function(){
    Route::resource('employees', App\Http\Controllers\UsersController::class);
});
