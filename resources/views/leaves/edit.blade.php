@extends('layouts.master')

@section('main-content')

<div class="card">
    <h5 class="card-header">Edit Leave</h5>
    <div class="card-body">
      <form method="post" action="{{route('leaves.update',$leave->id)}}">
        @csrf 
        @method('PATCH')
        <div class="form-group">
          <label for="inputTitle" class="col-form-label">Start Date</label>
        <input id="inputTitle" type="date" name="start_date" min="{{date('Y-m-d')}}" placeholder="Enter Start Date"  value="{{$leave->start_date}}" class="form-control">
        @error('start_date')
        <span class="text-danger">{{$message}}</span>
        @enderror
        </div>

        <div class="form-group">
          <label for="inputTitle" class="col-form-label">End  Date</label>
        <input id="inputTitle" type="date" name="end_date" min="{{date('Y-m-d')}}" placeholder="Enter End Date"  value="{{$leave->end_date}}" class="form-control">
        @error('end_date')
        <span class="text-danger">{{$message}}</span>
        @enderror
        </div>
        <div class="form-group">
            <label for="inputPassword" class="col-form-label">Reason</label>
            <textarea id="reason" name="reason" placeholder="Enter Reason" class="form-control">{{$leave->reason}}</textarea>
          @error('reason')
          <span class="text-danger">{{$message}}</span>
          @enderror
        </div>
        @if(Auth()->user()->role == 1)
         <div class="form-group">
            <label for="status" class="col-form-label">Status</label>
            <select name="status" class="form-control">
                <option value="approved" {{(($leave->status=='approved') ? 'selected' : '')}}>Approved</option>
                <option value="upnapproved" {{(($leave->status=='unapproved') ? 'selected' : '')}}>Unapproved</option>
            </select>
          @error('status')
          <span class="text-danger">{{$message}}</span>
          @enderror
          </div>
          @endif
        <div class="form-group mb-3">
           <button class="btn btn-success" type="submit">Update</button>
        </div>
      </form>
    </div>
</div>

@endsection

@push('scripts')
<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
<script>
    $('#lfm').filemanager('image');
</script>
@endpush