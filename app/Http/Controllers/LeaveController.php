<?php

namespace App\Http\Controllers;

use App\Models\Leave;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Notification;
use Illuminate\Support\Facades\Mail;
class LeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth()->user()->role == 1){
            $leaves=Leave::orderBy('id','DESC')->paginate(10);
        }else{
            $leaves=Leave::where('user_id',Auth()->user()->id)->orderBy('id','DESC')->paginate(10);
        }
        return view('leaves.index')->with('leaves',$leaves);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('leaves.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = Auth::User()->id;
        
        $adminUserID = User::where('role',1)->pluck('id')->first();
        $userName = Auth()->user()->name;
        $this->validate($request,
        [
            'start_date'=>'required',
            'end_date' => 'required|date|after_or_equal:start_date',
            'reason'=>'required',
        ]);
        // dd($request->all());
        $data=$request->all();
        $data['user_id']= isset($request->user_id) ? $request->user_id : $userId;
        // dd($data);
        $status=Leave::create($data);
        // dd($status);
        if($status){
            $notificationData = [
                'user_id' => $adminUserID,
                'data' => $userName." Applying for leave start date:".$request->start_date." To End Date:".$request->end_date.".",
            ];
            Notification::create($notificationData);

            //mail data to employee for leave status start code
            $user_name = '';
            $user_email = '';
            $user_data = User::select('name','email')->where('id',$userId)->first();
            $user_name = $user_data['name'];
            $data = [
                'email'   => $value = env('ADMIN_EMAIL'),
                'subject' => "Leave: ".$user_name." is apply for leave",
                'body'    =>  "Hello <b>Admin,</b><br><b>".$user_name."</b> is apply for leave start date:<b>".$request->start_date."</b> To End Date:<b>".$request->end_date.".</b>",
            ];

            LeaveController::sentMail($data);
            //mail data to employee for leave status end code
            request()->session()->flash('success','Successfully added leave');
        }
        else{
            request()->session()->flash('error','Error occurred while adding leave');
        }
        return redirect()->route('leaves.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $leave=Leave::findOrFail($id);
        return view('leaves.edit')->with('leave',$leave);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
        [
            'start_date'=>'required',
            'end_date' => 'required|date|after_or_equal:start_date',
            'reason'=>'required',
        ]);
       
        $leave=Leave::findOrFail($id);
         
        
        $data=$request->all();
        // dd($data);
        
        $status=$leave->fill($data)->save();
        if($status){
            if (Auth::user() &&  Auth::user()->role == 1) {
                //mail data to employee for leave status start code
                $user_name = '';
                $user_email = '';
                $user_data = User::select('name','email')->where('id',$leave->user_id)->first();
                $user_name = $user_data['name'];
                $user_email = $user_data['email'];
                if($leave->status == 'approved'){
                    $status = 'Approved';
                }else{
                    $status = 'Unapproved';
                }
                $data = [
                    'email'   => $user_email,
                    'subject' => "Leave:Your Leave is ".$status,
                    'body'    =>  "Hello <b>".$user_name.",</b><br> Your Leave is <b>".$status."</b>  for start date:<b>".$leave->start_date."</b> To End Date:<b>".$leave->end_date.".</b>",
                ];

                LeaveController::sentMail($data);
                $notificationData = [
                    'user_id' => $leave->user_id,
                    'data' => "Your leave is ".$status." for start date:".$leave->start_date." To End Date:".$leave->end_date.".",
                ];
                Notification::create($notificationData);
                //mail data to employee for leave status end code
            }
            request()->session()->flash('success','Successfully updated');
        }
        else{
            request()->session()->flash('error','Error occured while updating');
        }
        return redirect()->route('leaves.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete=Leave::findorFail($id);
        $status=$delete->delete();
        if($status){
            request()->session()->flash('success','Leave Successfully deleted');
        }
        else{
            request()->session()->flash('error','There is an error while deleting leave');
        }
        return redirect()->route('leaves.index');
    }
    public function sentMail($data){
      return $sentmail = Mail::send([], $data, function($message) use ($data)
        {
            $message->to($data['email'])
                    ->subject($data['subject']) 
                    ->setBody($data['body'], 'text/html');
        });
    }
    
}
