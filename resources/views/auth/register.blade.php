<!DOCTYPE html>
<html lang="en">

<head>
  <title>Registration Page</title>
  @include('layouts.head')

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9 mt-5">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
            <div class="col-lg-2"></div>
              <div class="col-lg-8">
                <div class="p-5">
                  <div class="text-center">
                    @include('layouts.notification')
                    <h1 class="h4 text-gray-900 mb-4">Registration</h1>
                  </div>
                  <form class="user"  method="POST" action="{{ route('employees_register') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" id="exampleInputName" aria-describedby="emailHelp" placeholder="Enter Employee Name..." autocomplete="name" autofocus>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                      <input type="email" class="form-control form-control-user @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address..." autocomplete="email" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" id="exampleInputPassword" placeholder="Password"  name="password" autocomplete="new-password">
                      @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" id="password-confirm" placeholder="Confirm Password"  name="password_confirmation" autocomplete="new-password">
                    </div>
                    <div class="form-group">
                      <input id="photo" type="file" name="photo" value="{{old('photo')}}" class="form-control form-control-user @error('photo') is-invalid @enderror">
                         @error('photo')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                      Register
                    </button>
                  </form>
                  <hr>
                   
                  <div class="text-center">
                    <a class="btn btn-link small" href="{{ route('login') }}">
                            Login?
                        </a>
                  </div>
                </div>
              </div>
              <div class="col-lg-2"></div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
</body>

</html>
