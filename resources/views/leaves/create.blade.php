@extends('layouts.master')

@section('main-content')

<div class="card">
    <h5 class="card-header">Add Leave</h5>
    <div class="card-body">
      <form method="post" action="{{route('leaves.store')}}">
        {{csrf_field()}}
        <div class="form-group">
          <label for="inputTitle" class="col-form-label">Start Date</label>
        <input id="start_date" type="date" name="start_date" min="{{date('Y-m-d')}}" placeholder="Enter Start Date"  value="{{old('start_date')}}" class="form-control">
        @error('start_date')
        <span class="text-danger">{{$message}}</span>
        @enderror
        </div>
        <div class="form-group">
            <label for="inputEmail" class="col-form-label">End Date</label>
          <input id="end_date" type="date" name="end_date" min="{{date('Y-m-d')}}" placeholder="Enter End Date"  value="{{old('end_date')}}" class="form-control">
          @error('end_date')
          <span class="text-danger">{{$message}}</span>
          @enderror
        </div>

        <div class="form-group">
            <label for="inputPassword" class="col-form-label">Reason</label>
            <textarea id="reason" name="reason" placeholder="Enter Reason"  value="{{old('reason')}}" class="form-control"></textarea>
          @error('reason')
          <span class="text-danger">{{$message}}</span>
          @enderror
        </div>
          <!-- <div class="form-group">
            <label for="status" class="col-form-label">Status</label>
            <select name="status" class="form-control">
                <option value="approved">Approved</option>
                <option value="unapproved">Unapproved</option>
            </select>
          @error('status')
          <span class="text-danger">{{$message}}</span>
          @enderror
          </div> -->
        <div class="form-group mb-3">
          <button type="reset" class="btn btn-warning">Reset</button>
           <button class="btn btn-success" type="submit">Submit</button>
        </div>
      </form>
    </div>
</div>

@endsection

@push('scripts')
<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
<script>
    $('#lfm').filemanager('image');
</script>
@endpush