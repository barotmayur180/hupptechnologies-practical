<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Leave;
use App\Models\Notification;
use Auth;
use File;
use Illuminate\Support\Facades\Storage;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
   
        $email = $request->input('email');
        $password = $request->input('password');
        $user = User::where('email', '=', $email)->first();
        
        if (!$user) {
            request()->session()->flash('error','Sorry! Not any record match with our records');
                return redirect()->route('login');
         }
         if (!Hash::check($password, $user->password)) {
            request()->session()->flash('error','Sorry! Please  check your password');
                return redirect()->route('login');
         }
        if ($user->status != 'active') {
            request()->session()->flash('error','Sorry! Account has been disabled. Contact administrator');
            return redirect()->route('login');
        }
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            request()->session()->flash('success','Successfully Login');
            return redirect()->route('home');
        }
    }

    public function index()
    {
        if(Auth()->user()->role == 1){
            $users=User::orderBy('id','DESC')->paginate(10);
            return view('employees.index')->with('users',$users);
        }else{
            $leaves=Leave::where('user_id',Auth()->user()->id)->orderBy('id','DESC')->paginate(10);
            return view('leaves.index')->with('leaves',$leaves);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        [
            'name'=>'string|required|max:30',
            'email'=>'string|required|unique:users',
            'password'=>'string|required',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        // dd($request->all());
        $data=$request->all();
        $data['password']=Hash::make($request->password);
        
        // dd($data);
        //$path = $request->file('photo')->store('photo');
        //Storage::disk('local')->put($request->file('photo'), 'Contents');
        $image_custom_name = "";
        $image_fullpath = "";
        if (isset($request->photo) && !empty($request->photo)) {
            $image_upload_dir  = public_path() . '/uploads/users_images/';
            File::isDirectory($image_upload_dir) or File::makeDirectory($image_upload_dir, 0777, true, true);

            $errors     = array();

            $image_temp_path = $request->photo->getRealPath();
            $image_file_name = $request->photo->getClientOriginalName();
            $image_file_ext  = $request->photo->getClientOriginalExtension();
            $image_file_size = $request->file('photo')->getSize();

            $image_custom_name = time() . "." . $image_file_ext;
            if (!empty($image_custom_name) && $image_custom_name != '') {
                $image_fullpath = '/users_images/'. $image_custom_name;
            }
            if (empty($errors) == true) {
                $request->photo->move($image_upload_dir, $image_custom_name);
                $data['photo'] = $image_file_name;
                $data['photo_path'] = $image_fullpath;
            } else {
                return false;
            }
        }
        $status=User::create($data);
        // dd($status);
        if($status){
            request()->session()->flash('success','Successfully added user');
        }
        else{
            request()->session()->flash('error','Error occurred while adding user');
        }
        return redirect()->route('employees.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=User::findOrFail($id);
        return view('employees.edit')->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $user=User::findOrFail($id);
        $this->validate($request,
        [
            'name'=>'string|required|max:30',
            'email'=>'string|required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        
        $data=$request->all();
         //dd($data);
        $image_custom_name = "";
        $image_fullpath = "";
        if (isset($request->photo) && !empty($request->photo)) {
            $image_upload_dir  = public_path() . '/uploads/users_images/';
            File::isDirectory($image_upload_dir) or File::makeDirectory($image_upload_dir, 0777, true, true);

            $errors     = array();

            $image_temp_path = $request->photo->getRealPath();
            $image_file_name = $request->photo->getClientOriginalName();
            $image_file_ext  = $request->photo->getClientOriginalExtension();
            $image_file_size = $request->photo->getSize();

            $image_custom_name = time() . "." . $image_file_ext;
            if (!empty($image_custom_name) && $image_custom_name != '') {
                $image_fullpath = '/users_images/'. $image_custom_name;
            }
            if (empty($errors) == true) {
                $request->photo->move($image_upload_dir, $image_custom_name);
                $data['photo'] = $image_file_name;
                $data['photo_path'] = $image_fullpath;
            } else {
                return false;
            }
        }
        $status=$user->fill($data)->save();
        if($status){
            request()->session()->flash('success','Successfully updated');
        }
        else{
            request()->session()->flash('error','Error occured while updating');
        }
        return redirect()->route('employees.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete=User::findorFail($id);
        $status=$delete->delete();
        if($status){
            request()->session()->flash('success','User Successfully deleted');
        }
        else{
            request()->session()->flash('error','There is an error while deleting users');
        }
        return redirect()->route('employees.index');
    }
    public function profile(){
        $profile=Auth()->user();
        // return $profile;
        return view('employees.profile')->with('profile',$profile);
    }

    public function profileUpdate(Request $request,$id){
        $this->validate($request,
        [
            'name'=>'string|required|max:30',
            'email'=>'string|required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        // return $request->all();
        $user=User::findOrFail($id);
        $data=$request->all();
        $image_custom_name = "";
        $image_fullpath = "";
        if (isset($request->photo) && !empty($request->photo)) {
            $image_upload_dir  = public_path() . '/uploads/users_images/';
            File::isDirectory($image_upload_dir) or File::makeDirectory($image_upload_dir, 0777, true, true);

            $errors     = array();

            $image_temp_path = $request->photo->getRealPath();
            $image_file_name = $request->photo->getClientOriginalName();
            $image_file_ext  = $request->photo->getClientOriginalExtension();
            $image_file_size = $request->file('photo')->getSize();

            $image_custom_name = time() . "." . $image_file_ext;
            if (!empty($image_custom_name) && $image_custom_name != '') {
                $image_fullpath = '/users_images/'. $image_custom_name;
            }
            if (empty($errors) == true) {
                $request->photo->move($image_upload_dir, $image_custom_name);
                $data['photo'] = $image_file_name;
                $data['photo_path'] = $image_fullpath;
            } else {
                return false;
            }
        }
        $status=$user->fill($data)->save();
        if($status){
            request()->session()->flash('success','Successfully updated your profile');
        }
        else{
            request()->session()->flash('error','Please try again!');
        }
        return redirect()->back();
    }
    public function register(Request $request)
    {
        $this->validate($request,
        [
            'name'=>'string|required|max:30',
            'email'=>'string|required|unique:users',
            'password'=>'string|required|confirmed|min:8',
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        // dd($request->all());
        $data=$request->all();
        $data['password']=Hash::make($request->password);
        // dd($data);
        $image_custom_name = "";
        $image_fullpath = "";
        if (isset($request->photo) && !empty($request->photo)) {
            $image_upload_dir  = public_path() . '/uploads/users_images/';
            File::isDirectory($image_upload_dir) or File::makeDirectory($image_upload_dir, 0777, true, true);

            $errors     = array();

            $image_temp_path = $request->photo->getRealPath();
            $image_file_name = $request->photo->getClientOriginalName();
            $image_file_ext  = $request->photo->getClientOriginalExtension();
            $image_file_size = $request->file('photo')->getSize();

            $image_custom_name = time() . "." . $image_file_ext;
            if (!empty($image_custom_name) && $image_custom_name != '') {
                $image_fullpath = '/users_images/'. $image_custom_name;
            }
            if (empty($errors) == true) {
                $request->photo->move($image_upload_dir, $image_custom_name);
                $data['photo'] = $image_file_name;
                $data['photo_path'] = $image_fullpath;
            } else {
                return false;
            }
        }
        $status=User::create($data);
        // dd($status);
        if($status){
            request()->session()->flash('success','Registration Successfully done');
            return redirect()->route('login');
        }
        else{
            request()->session()->flash('error','Error occurred while registration');
            return redirect()->back();
        }
    }
    public function notificationList()
    {
        $notifications=Notification::where('user_id',Auth()->user()->id)->orderBy('id','DESC')->paginate(10);
        return view('employees.notification')->with('notifications',$notifications);
    }
}
