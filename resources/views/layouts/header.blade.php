<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link  rounded-circle mr-3">
      <i class="fa fa-bars"></i>
    </button>
    <ul class="notificationsbtn nav navbar-nav navbar-right m-left">
                <li id="notificationsli">
                    <a id="notifications" href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i>
                  </a>
                   @php
                      $notifications=DB::table('notifications')->where('user_id',Auth()->user()->id)->whereNull('read_at')->orderBy('id','DESC')->limit(5)->get();
                    @endphp
                    <div id="notification-container" class="dropdown-menu noti" role="menu" aria-labelledby="drop3">

                        <section class="panel">
                            <header class="panel-heading">
                                <strong>Notifications</strong>
                            </header>
                            <div id="notification-list" class="list-group list-group-alt">
                              @if(isset($notifications) && !empty($notifications) && count($notifications) > 0)
                              @foreach($notifications as $notification)
                              <div style="">
                                <div class="noty-manager-list-item noty-manager-list-item-error">
                                  <div class="activity-item"> 
                                    <i class="fa fa-bell text-success"></i> 
                                    <div class="activity"> 
                                      {{$notification->data}}
                                      <span>{{ \Carbon\Carbon::parse($notification->created_at)->diffForHumans() }}</span> 
                                    </div> 
                                  </div>
                                </div>
                                @endforeach
                                @else
                                <div style="">
                                <div class="noty-manager-list-item noty-manager-list-item-error">
                                  <div class="activity-item"> 
                                    <i class="fa fa-bell text-success"></i> 
                                    <div class="activity"> 
                                      No record found
                                    </div> 
                                  </div>
                                </div>
                                @endif
                              </div>      
                            </div>
                            @if(isset($notifications) && !empty($notifications) && count($notifications) > 0)
                            <footer class="panel-footer">
                                <a href="{{ route('notificationList') }}" class="pull-right"><i class="fa fa-bell"></i></a>
                                <a href="{{ route('notificationList') }}" data-toggle="class:show animated fadeInRight">See all the notifications</a>
                            </footer>
                            @endif
                        </section>

                    </div>
                </li>
            </ul>
    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto ">
      <div class="topbar-divider d-none d-sm-block"></div>
      

      <!-- Nav Item - User Information -->
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ isset(Auth()->user()->name) ? Auth()->user()->name :'Admin'}}</span>
          @if(isset(Auth()->user()->photo))
            <img class="img-profile rounded-circle" src="{{url('uploads/' . Auth()->user()->photo_path)}}">
          @else
            <img class="img-profile rounded-circle" src="{{asset('backend/img/avatar.png')}}">
          @endif
        </a>
        <!-- Dropdown - User Information -->
        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
        <a class="dropdown-item" href="{{route('profile')}}">
            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
            Profile
          </a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                 <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i> {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
      </li>

    </ul>

  </nav>