<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
      <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
      </div>
      
      <div class="sidebar-brand-text mx-3">{{ ((isset(Auth()->user()->role)) && (Auth()->user()->role == 1))? 'Admin' :'User'}}</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
      <a class="nav-link" href="#">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Employee
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    @if(isset(Auth()->user()->role) && Auth()->user()->role == 1)
    <li class="nav-item">
      <a class="nav-link collapsed" href="{{ url('employees') }}" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        <i class="fas fa-users"></i>
        <span>Employee </span>
      </a>
      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Employee Options:</h6>
          <a class="collapse-item" href="{{ url('employees') }}">Employees </a>
          <a class="collapse-item" href="{{route('employees.create')}}">Add Employee</a>
        </div>
      </div>
    </li>
    @endif
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ url('leaves') }}" data-toggle="collapse" data-target="#categoryCollapse" aria-expanded="true" aria-controls="categoryCollapse">
          <i class="fas fa-sitemap"></i>
          <span>Leaves</span>
        </a>
        <div id="categoryCollapse" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Leaves Options:</h6>
            <a class="collapse-item" href="{{ url('leaves') }}">Leaves</a>
            @if(Auth()->user()->role != 1)
            <a class="collapse-item" href="{{ route('leaves.create') }}">Add Leaves</a>
            @endif
          </div>
        </div>
    </li>

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>