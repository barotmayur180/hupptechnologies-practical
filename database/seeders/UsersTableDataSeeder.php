<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use DB;
class UsersTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=array(
            array(
                'name'=>'Admin',
                'email'=>'admin@gmail.com',
                'password'=>Hash::make('Admin@123'),
                'role'=>'1',
                'status'=>'active'
            ),
            array(
                'name'=>'Employee',
                'email'=>'employee@gmail.com',
                'password'=>Hash::make('Employee@123'),
                'role'=>'2',
                'status'=>'active'
            ),
        );

        DB::table('users')->insert($data);
    }
}
